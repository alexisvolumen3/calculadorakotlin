package com.example.examencorte1

class Calculadora {
    var num1: Float = 0.0f
    var num2: Float = 0.0f

    constructor()

    constructor(num1: Float, num2: Float) {
        this.num1 = num1
        this.num2 = num2
    }

    fun suma(): Float {
        return this.num1 + this.num2
    }

    fun resta(): Float {
        return this.num1 - this.num2
    }

    fun multiplicacion(): Float {
        return this.num1 * this.num2
    }

    fun division(): Float {
        return this.num1 / this.num2
    }
}
