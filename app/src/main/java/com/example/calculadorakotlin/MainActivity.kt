package com.example.calculadorakotlin

import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private lateinit var lblCalculadora: TextView
    private lateinit var lblUsuario: TextView
    private lateinit var lblContraseña: TextView
    private lateinit var txtUsuario: EditText
    private lateinit var txtContraseña: EditText
    private lateinit var btnIngresar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lblCalculadora = findViewById(R.id.lblcalc)
        lblUsuario = findViewById(R.id.lblUser)
        lblContraseña = findViewById(R.id.lblContra)
        txtUsuario = findViewById(R.id.txtUsuario)
        txtContraseña = findViewById(R.id.txtContra)
        btnIngresar = findViewById(R.id.btnIng)
        btnSalir = findViewById(R.id.btnSalir)

        btnIngresar.setOnClickListener {
            when {
                txtUsuario.text.toString().isEmpty() || txtContraseña.text.toString().isEmpty() -> {
                    Toast.makeText(this, "Falto capturar información", Toast.LENGTH_SHORT).show()
                }
                txtUsuario.text.toString() == getString(R.string.user) && txtContraseña.text.toString() == getString(R.string.pass) -> {
                    val iCalc = Intent(this, CalculadoraActivity::class.java)
                    iCalc.putExtra("Nombre", txtUsuario.text.toString())
                    startActivity(iCalc)
                }
                else -> {
                    Toast.makeText(this, "Contraseña o Usuario incorrecto", Toast.LENGTH_SHORT).show()
                }
            }
        }

        btnSalir.setOnClickListener {
            val dlg = AlertDialog.Builder(this)
            dlg.setTitle("¿Desea salir de la aplicación?")
            dlg.setMessage("Presione OK para cerrar la aplicación")
                .setCancelable(false)
                .setPositiveButton("OK") { _, _ ->
                    finish()
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }
            val alertDialog = dlg.create()
            alertDialog.show()
        }
    }
}
