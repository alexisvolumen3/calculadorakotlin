package com.example.calculadorakotlin

import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.examencorte1.Calculadora

class CalculadoraActivity : AppCompatActivity() {

    private lateinit var lblCalcalculadora: TextView
    private lateinit var lblUsuario: TextView
    private lateinit var lblNum1: TextView
    private lateinit var lblNum2: TextView
    private lateinit var lblResultado: TextView
    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText
    private lateinit var btnSuma: Button
    private lateinit var btnResta: Button
    private lateinit var btnMultiplicacion: Button
    private lateinit var btnDivision: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button
    private lateinit var calculadora: Calculadora

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)

        lblCalcalculadora = findViewById(R.id.lblCalcalculador)
        lblUsuario = findViewById(R.id.lblUsuario)
        lblNum1 = findViewById(R.id.lblNum1)
        lblNum2 = findViewById(R.id.lblNum2)
        lblResultado = findViewById(R.id.lblResultado)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        btnSuma = findViewById(R.id.btnSuma)
        btnDivision = findViewById(R.id.btnDivision)
        btnResta = findViewById(R.id.btnResta)
        btnMultiplicacion = findViewById(R.id.btnMultiplicacion)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        calculadora = Calculadora()

        val datos = intent.extras
        val nombre = datos?.getString("Nombre")
        lblUsuario.text = nombre

        btnRegresar.setOnClickListener {
            finish()
        }

        btnLimpiar.setOnClickListener {
            txtNum1.text.clear()
            txtNum2.text.clear()
            lblResultado.text = "Resultado: "
        }

        btnSuma.setOnClickListener {
            if (txtNum1.text.toString().isEmpty() || txtNum2.text.toString().isEmpty()) {
                Toast.makeText(this, "Falto capturar información", Toast.LENGTH_SHORT).show()
            } else {
                setNumbers()
                lblResultado.text = "Resultado: ${calculadora.suma()}"
            }
        }

        btnResta.setOnClickListener {
            if (txtNum1.text.toString().isEmpty() || txtNum2.text.toString().isEmpty()) {
                Toast.makeText(this, "Falto capturar información", Toast.LENGTH_SHORT).show()
            } else {
                setNumbers()
                lblResultado.text = "Resultado: ${calculadora.resta()}"
            }
        }

        btnMultiplicacion.setOnClickListener {
            if (txtNum1.text.toString().isEmpty() || txtNum2.text.toString().isEmpty()) {
                Toast.makeText(this, "Falto capturar información", Toast.LENGTH_SHORT).show()
            } else {
                setNumbers()
                lblResultado.text = "Resultado: ${calculadora.multiplicacion()}"
            }
        }

        btnDivision.setOnClickListener {
            if (txtNum1.text.toString().isEmpty() || txtNum2.text.toString().isEmpty()) {
                Toast.makeText(this, "Falto capturar información", Toast.LENGTH_SHORT).show()
            } else if (txtNum2.text.toString().toFloat() == 0f){
                Toast.makeText(this, "No se puede dividir entre 0", Toast.LENGTH_SHORT).show()
            } else {
                setNumbers()
                lblResultado.text = "Resultado: ${calculadora.division()}"
            }
        }
    }

    private fun setNumbers() {
        calculadora.num1 = txtNum1.text.toString().toFloat()
        calculadora.num2 = txtNum2.text.toString().toFloat()
    }
}
